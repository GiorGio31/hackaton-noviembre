import {LitElement, html} from 'lit-element';

class CochesListadoCochesDM extends LitElement {
    
    static get properties(){
        return {
            coches: {type: Array},
            coches2: {type: Array}
        };
    }

    constructor(){
        super();

        this.coches2 = [];
        this.coches = [
            {
                marca: "Ford",
                modelo: "Focus",
                matricula: "1234BBB",
                disponible: true,
                imagen: {
                    src: "./img/focus.jpg",
                    alt: "fotofocus"
                },
                sede: "Ciudad BBVA",
                carga: 100
                
            },
            {
                marca: "Seat",
                modelo: "Leon",
                matricula: "5678CCC",
                disponible: true,
                imagen: {
                    src: "./img/leon.jpg",
                    alt: "fotoleon"
                },
                sede: "Las Tablas 2",
                carga: 80
                
            },
            {
                marca: "Peugeot",
                modelo: "208",
                matricula: "9012DDD",
                disponible: false,
                imagen: {
                    src: "./img/208.jpg",
                    alt: "foto208"
                },
                sede: "Ciudad BBVA",
                carga: 70
                
            },
            {
                marca: "Ferrari",
                modelo: "488",
                matricula: "3456FFF",
                disponible: true,
                imagen: {
                    src: "./img/ferrari.jpg",
                    alt: "fotoferrari"
                },
                sede: "Tres Cantos",
                carga: 60
                
            }

        ];

        //this.getCochesData();
    }

    getCochesData(){
        console.log("Obteniendo datos de coches...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición GET coches completada OK");
                this.coches = JSON.parse(xhr.responseText);
            }

        }
        xhr.open("GET", "http://localhost:8081/hackathon/v1/coches");
        xhr.send();
        console.log("Fin de getCochesData");
    }
    
    updated(changedProperties){
        if(changedProperties.has("coches")){

            this.dispatchEvent(new CustomEvent("carga-datos-coches", {
                detail: {
                    coches: this.coches
                }
            }))
        }
    }
}

customElements.define('coches-listadocoches-dm', CochesListadoCochesDM);