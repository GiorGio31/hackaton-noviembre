import {LitElement, html} from 'lit-element';

class CochesHeader extends LitElement {
    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">
            <h2 class="text-center">Aplicación Reserva de Coches BBVA</h2>
            <div>
                <div class="mt-5">
                    <button class="w-30 btn btn-primary" style="font-size:30px" @click="${this.nuevaReserva}"><strong>Nueva Reserva</strong></button>
                    <button class="w-30 btn btn-primary" style="font-size:30px" @click="${this.consultaReserva}"><strong>Consulta Reserva</strong></button>
                    <button class="w-30 btn btn-primary" style="font-size:30px" @click="${this.consultaCoches}"><strong>Consulta Coches</strong></button>
                </div>
            </div>
        `;
    }

    nuevaReserva(){
        this.dispatchEvent(new CustomEvent("nueva-reserva", {}))
    }
    consultaReserva(){
        this.dispatchEvent(new CustomEvent("consulta-reserva", {}))
    }
    consultaCoches(){
        this.dispatchEvent(new CustomEvent("consulta-coches", {}))
    }
}

customElements.define('coches-header', CochesHeader);