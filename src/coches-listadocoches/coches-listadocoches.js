import {LitElement, html} from 'lit-element';

import '../coches-listadocoches-dm/coches-listadocoches-dm.js';
import '../coches-coche/coches-coche.js';


class CochesListadoCoches extends LitElement {
    
    static get properties(){
        return {
            coches: {type: Array},
            showCoches: {type: Boolean},
            filtrosActivos: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.coches = [];
        this.showCoches=false;
        this.filtrosActivos=false;
    }
    
    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">

            <div class="row" id="listadoCoches">
                <div class="row row-cols-1 row-cols-md-5">
                    ${this.coches.map( coche => this.filtros(coche) ? html`<coches-coche 
                        marca="${coche.marca}" 
                        modelo="${coche.modelo}"
                        matricula="${coche.matricula}"
                        .disponible="${coche.disponible}"
                        .imagen="${coche.imagen}"
                        sede="${coche.sede}"
                        carga="${coche.carga}"
                        @coche-seleccionado="${this.cocheSeleccionado}"
                        ></coches-coche>` : null)}
                </div>
            </div>
        `;
    }

    cocheSeleccionado(e){
        this.dispatchEvent(new CustomEvent("coche-seleccionado", {
            detail: {
                coche: e.detail.coche
            }
        }))
    }

    filtros(coche){
        if(this.filtrosActivos){
            return coche.disponible;
        }
        return true;

    }

    cargaDatosCoches(e){
        console.log("Datos de coches cargados");
        this.coches = e.detail.coches;
        console.log(this.coches);
    }

    updated(changedProperties){
        if(changedProperties.has("showCoches")){
            this.toggleShowListado();
        }

        if(changedProperties.has("coches")){
            this.dispatchEvent(new CustomEvent("updated-coches", {
                detail: {
                    coches: this.coches
                }
            }))
        }
    }

    toggleShowListado(){
        if(this.showCoches)
            this.shadowRoot.getElementById("listadoCoches").classList.remove("d-none"); 
        else
            this.shadowRoot.getElementById("listadoCoches").classList.add("d-none");
    }


}

customElements.define('coches-listadocoches', CochesListadoCoches);