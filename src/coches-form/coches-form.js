import {LitElement, html} from 'lit-element';


class CochesForm extends LitElement {

    static get properties(){
        return{
            coches: {type: Array},
            buildings: {type: Array},
            buildings2: {type: Array},
            destbuildings: {type: Array},
            showBookingForm: {type: Boolean},
            dateSearch: {type: Date},
            cocheSelect: {type: Object},
            showForm: {type: Boolean},
            filtrosActivos: {type: Boolean}
        }    
    }

    constructor(){
        super();
        this.destbuildings = ["Las Tablas I", "Las Tablas II", "Las Tablas III", "Ciudad BBVA", "Tres Cantos", "Vaguada"];
        this.buildings2 = [];
        this.buildings = [
            {
                id: "01",
                name:"Las Tablas I"
            },
            {
                id: "02",
                name:"Las Tablas II"
            },
            {
                id:"03",
                name:"Las Tablas III"
            },
            {
                id:"04",
                name:"Ciudad BBVA"
            },
            {
                id:"05",
                name:"Tres Cantos"
            },
            {
                id:"06",
                name:"Vaguada"
            }
        ];
        this.cocheSelect ={};
        this.cocheSelect.marca ="";
        this.cocheSelect.modelo ="";
        this.cocheSelect.matricula ="";

        this.showForm = false;
        this.coches2 = [];

        this.coches = [
            {
                marca: "Ford",
                modelo: "Focus",
                matricula: "1234BBB",
                disponible: true,
                imagen: "./img/focus.jpg",
                sede: "Ciudad BBVA",
                carga: 100
                
            },
            {
                marca: "Seat",
                modelo: "Leon",
                matricula: "5678CCC",
                disponible: true,
                imagen: "./img/leon.jpg",
                sede: "Las Tablas 2",
                carga: 80
                
            },
            {
                marca: "Peugeot",
                modelo: "208",
                matricula: "9012DDD",
                disponible: false,
                imagen: "./img/208.jpg",
                sede: "Ciudad BBVA",
                carga: 70
                
            },
            {
                marca: "Ferrari",
                modelo: "488",
                matricula: "3456FFF",
                disponible: true,
                imagen: "./img/ferrari.jpg",
                sede: "Tres Cantos",
                carga: 60
                
            }

        ];

        //this.getBuildings();
        

    }

    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        
        <form id="formulario">
        Consulta vehículos disponibles:  
        <br>
        <br>    
            <div class="form-row">
                <div class="col-md-2">
                    <label>Sede de Vehículo</label>
                    <select class="form-control" id="originBuilding" @input="${this.selectedOriginBuild}">
                        <option>Selecciona...</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label">Fecha de desplazamiento</label>
                    <input class="form-control" type="date" id="ffecha" @input="${this.updateDate}" disabled/>
                </div>
                <div class="col-md-2">  
                    <label for="ocultaReservado">No mostrar vehículos reservados</label>
                    <input type="checkbox" id="ocultaReservado"/>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" id="bListarCoches" @click="${this.searchCarsBack}" disabled>Buscar Coches</button>
                </div>
            </div>

            <div class="form-row" id="camposReserva">
                <div class="col-md-2">
                    <label>Sede Destino</label>
                    <select class="form-control" id="destBuilding">
                        <option>Selecciona...</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Vehículo seleccionado</label>
                    <input type="text" class="form-control" disabled .value="${this.cocheSelect.marca} ${this.cocheSelect.modelo} - ${this.cocheSelect.matricula}"/>
                </div>
                <div class="col-md-2">
                    <label>Titular de la reserva</label>
                    <input type="text" id="fPersona" class="form-control"/>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" @click="${this.book}">Reservar!</button>
                </div>
            </div>
        </form>

        
         
        `;
    }

    updated(changedProperties){
        if(changedProperties.has("buildings")){
            this.updateOriginBuildings();       
        } 
        if(changedProperties.has("destbuildings")){
            this.updateDestBuildings();       
        } 
        if(changedProperties.has("showForm")){
            this.toggleShowForm();

        }
    }

    toggleShowForm(){
        if(this.showForm)
            this.shadowRoot.getElementById("formulario").classList.remove("d-none"); 
        else
        {
            this.shadowRoot.getElementById("formulario").classList.add("d-none");
            this.shadowRoot.getElementById("camposReserva").classList.add("d-none");
        }
    }

    getBuildings(){
        console.log("Obteniendo datos de edificios...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición GET edificios completada OK");
                this.buildings = JSON.parse(xhr.responseText);
            }

        }
        //let edif = {sede: this.shadowRoot.getElementById("originBuilding").value}
        //xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.open("GET", "http://localhost:8080/hackathon/v1/sedes");
        //xhr.send(JSON.stringify(edif));
        console.log("Fin de getBuildings");

    }


    book(e){
        e.preventDefault();
        console.log("Se va a realizar una reserva: ");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición POST reservas completada OK");
            }

        }
        let reserva = {
            id: "9999",
            fecha: this.shadowRoot.getElementById("ffecha").value,
            origen: this.shadowRoot.getElementById("originBuilding").value,
            destin: this.shadowRoot.getElementById("destBuilding").value,
            matricula: this.cocheSelect.matricula,
            nombre: this.shadowRoot.getElementById("fPersona").value,
        };
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.open("POST", "http://localhost:8080/hackathon/v1/reservas");
        //xhr.send(JSON.stringify(reserva));
        console.log("Fin de book");
        
    }

    searchCarsBack(e){
        e.preventDefault();
        console.log("Se ha pulsado buscar coches");

        //this.filtrosActivos = this.shadowRoot.getElementById("ocultaReservado").checked;
        //this.getCochesData();

        this.dispatchEvent(new CustomEvent("carga-listado-coches", {
            detail: {
                filtrosActivos: this.filtrosActivos,
                coches: this.coches
            }
        }))

        this.shadowRoot.getElementById("camposReserva").classList.remove("d-none"); 
 
        
    }

    getCochesData(){
        console.log("Obteniendo datos de coches...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición GET coches completada OK");
                this.coches = JSON.parse(xhr.responseText);
            }

        }
        xhr.open("GET", "http://localhost:8080/hackathon/v1/vehiculos");
        xhr.send();
        console.log("Fin de getCochesData");
    }


    searchCars(e){
        e.preventDefault();
        console.log("Se ha pulsado buscar coches");

        this.filtrosActivos = this.shadowRoot.getElementById("ocultaReservado").checked;

        this.dispatchEvent(new CustomEvent("carga-listado-coches", {
            detail: {
                filtrosActivos: this.filtrosActivos
            }
        }))
    }

    //Edificio seleccionado
    selectedOriginBuild(e){
        this.shadowRoot.getElementById("ffecha").disabled = false;
    }

    //recoger fecha insertada
    updateDate(e){
        console.log("Se ha insertado una fecha: " + e.target.value);
        this.dateSearch = e.target.value;
        this.shadowRoot.getElementById("bListarCoches").disabled = false;
    }

    //Cargar elemento (select) de listado de edificios de origen y destino
    updateOriginBuildings(){
        //Recorremos el array.
        this.buildings.forEach(build => {
            var opt = document.createElement('option');
            opt.name = build.name;
            opt.innerHTML = build.name;
            this.shadowRoot.getElementById("originBuilding").appendChild(opt);
        });

    }
    updateDestBuildings(){

        //Recorremos el array.
        this.destbuildings.forEach(build => {
            var opt = document.createElement('option');
            opt.name = build;
            opt.innerHTML = build;
            this.shadowRoot.getElementById("destBuilding").appendChild(opt);
        });

    }

}

customElements.define('coches-form', CochesForm);