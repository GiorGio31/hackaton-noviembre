import {LitElement, html} from 'lit-element';

class CochesCoche extends LitElement {

    static get properties(){
        return {
            marca: {type: String},
            modelo: {type: String},
            matricula: {type: String},
            disponible: {type: Boolean},
            imagen: {type: Object},
            sede: {type: String},
            carga: {type: Number}
        }
    }

    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
        crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.imagen}" height="200" width="100" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${this.marca} ${this.modelo}</h5>
                    <h6 class="card-subtitle">${this.matricula}</h6>
                    <h6 class="card-subtitle">Ubicación: ${this.sede}</h6>
                    <h6 class="card-subtitle">Carga: ${this.carga}%</h6>
                    <br>
                    <h6 class="card-subtitle" id="dispon"></h6>
                </div>
                <div class="card-footer">
                    <button class="btn btn-info" @click="${this.seleccionarCoche}">Seleccionar</button>
                </div>
                </div>
            </div>
        `;
    }
    
    disponibilidad(){
        if(this.disponible)
        {
            this.shadowRoot.getElementById("dispon").classList.add("text-success");
            this.shadowRoot.getElementById("dispon").innerHTML = "Disponible";
        }
        else
        {
            this.shadowRoot.getElementById("dispon").classList.add("text-danger");
            this.shadowRoot.getElementById("dispon").innerHTML = "Reservado";
        }
    }

    updated(changedProperties){
        if(changedProperties.has("disponible")){

            this.disponibilidad();
        }
    }

    seleccionarCoche(){
        console.log("Coche seleccionado, emitiendo evento")

        let coche = {
            marca: this.marca,
            modelo: this.modelo,
            sede: this.sede,
            carga: this.carga,
            matricula: this.matricula
        }

        this.dispatchEvent(new CustomEvent("coche-seleccionado", {
            detail: {
                coche: coche
            }
        }))

    }
}

customElements.define('coches-coche', CochesCoche);