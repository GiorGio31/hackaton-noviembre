import {LitElement, html} from 'lit-element';

import '../coches-header/coches-header.js';
import '../coches-form/coches-form.js';
import '../coches-listadocoches/coches-listadocoches.js';
import '../coches-listadoreservas/coches-listadoreservas.js';
import '../coches-stats/coches-stats.js';
import '../coches-carrusel/coches-carrusel.js';

class CochesApp extends LitElement {
    render(){
        return html`
            <coches-header 
                @nueva-reserva="${this.nuevaReserva}" 
                @consulta-reserva="${this.consultaReserva}"
                @consulta-coches="${this.consultaCoches}"
            ></coches-header>
            <br>
            <coches-form @carga-listado-coches="${this.cargaListadoCoches}"></coches-form>
            <br>
            <coches-listadocoches @coche-seleccionado="${this.cocheSeleccionado}" @updated-coches="${this.updatedCoches}"></coches-listadocoches>
            <br>
            <coches-listadoreservas></coches-listadoreservas>
            <coches-stats @updated-propiedades-coches="${this.updatedPropiedadesCoches}"></coches-stats>
            <coches-carrusel></coches-carrusel>
        `;
    }

    cargaListadoCoches(e){
        this.shadowRoot.querySelector("coches-listadocoches").showCoches = true;
        this.shadowRoot.querySelector("coches-listadocoches").filtrosActivos = e.detail.filtrosActivos;
        this.shadowRoot.querySelector("coches-listadocoches").coches = e.detail.coches;

    }

    updatedPropiedadesCoches(e){
        this.shadowRoot.querySelector("coches-form").buildings = e.detail.propiedadesCoches.edificios;
    }

    cocheSeleccionado(e){
        console.log("Coche seleccionado")
        this.shadowRoot.querySelector("coches-form").cocheSelect = e.detail.coche;
    }

    updatedCoches(e){
        console.log("Cambia coches y estoy pasando por coches-app");
        this.shadowRoot.querySelector("coches-stats").coches = e.detail.coches;

    }

    nuevaReserva(){
        this.shadowRoot.querySelector("coches-form").showForm = true;
        this.shadowRoot.querySelector("coches-listadocoches").showCoches = false;
        this.shadowRoot.querySelector("coches-listadoreservas").showReservas = false;

    }
    consultaReserva(){
        this.shadowRoot.querySelector("coches-form").showForm = false;
        this.shadowRoot.querySelector("coches-listadocoches").showCoches = false;
        this.shadowRoot.querySelector("coches-listadoreservas").showReservas = true;

    }
    consultaCoches(){
        this.shadowRoot.querySelector("coches-form").showForm = false;
        this.shadowRoot.querySelector("coches-listadocoches").showCoches = false;
        this.shadowRoot.querySelector("coches-listadoreservas").showReservas = false;

    }
}

customElements.define('coches-app', CochesApp);