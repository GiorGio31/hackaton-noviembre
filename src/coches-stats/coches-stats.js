import {LitElement, html} from 'lit-element';


class CochesStats extends LitElement {
    
    static get properties(){
        return {
            coches: {type: Array},
            propiedadesCoches: {type: Object}
        };
    }

    constructor(){
        super();
        this.coches = [];
        this.propiedadesCoches = {};
    }

    updated(changedProperties){
        if(changedProperties.has("coches")){
            console.log("cambia coche y voy a calcular propiedades");
            this.calculaPropiedadesCoches();
        }

    }

    calculaPropiedadesCoches(){

        //Obtengo Listado de edificios donde están los coches
        let edificios = [];
        this.coches.forEach(coche => { edificios.push(coche.sede)  });
        this.propiedadesCoches.edificios = edificios.filter((item,index) => { return edificios.indexOf(item) === index});


        this.dispatchEvent(new CustomEvent("updated-propiedades-coches", {
            detail: {
                propiedadesCoches: this.propiedadesCoches
            }
        }))
    }



    
}

customElements.define('coches-stats', CochesStats);