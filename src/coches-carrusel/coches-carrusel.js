import {LitElement, html} from 'lit-element';

import '../coches-coche/coches-coche.js';


class CochesCarrusel extends LitElement {
    
    static get properties(){
        return {
            coches: {type: Array},
            showCarrusel: {type: Boolean},
        };
    }

    constructor(){
        super();

        this.coches2 = [];
        this.coches = [
            {
                marca: "Ford",
                modelo: "Focus",
                matricula: "1234BBB",
                disponible: true,
                imagen: {
                    src: "./img/focus.jpg",
                    alt: "fotofocus"
                },
                sede: "Ciudad BBVA",
                carga: 100
                
            },
            {
                marca: "Seat",
                modelo: "Leon",
                matricula: "5678CCC",
                disponible: true,
                imagen: {
                    src: "./img/leon.jpg",
                    alt: "fotoleon"
                },
                sede: "Las Tablas 2",
                carga: 80
                
            },
            {
                marca: "Peugeot",
                modelo: "208",
                matricula: "9012DDD",
                disponible: false,
                imagen: {
                    src: "./img/208.jpg",
                    alt: "foto208"
                },
                sede: "Ciudad BBVA",
                carga: 70
                
            },
            {
                marca: "Ferrari",
                modelo: "488",
                matricula: "3456FFF",
                disponible: true,
                imagen: {
                    src: "./img/ferrari.jpg",
                    alt: "fotoferrari"
                },
                sede: "Tres Cantos",
                carga: 60
                
            }

        ];
        this.showCarrusel=false;
        //this.getCochesData();
    }
    
    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">

            <div class="row" id="carruselCoches">

                <div id="carrusel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carrusel" data-slide-to="0" class="active"></li>
                        <li data-target="#carrusel" data-slide-to="1"></li>
                        <li data-target="#carrusel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">

                        ${this.coches.map( coche => html`
                        <div class="carousel-item active">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>${coche.marca} ${coche.modelo}</h5>
                                <p>${coche.matricula}</p>
                            </div>
                        </div>`)}

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>  
                </div>

            </div>
 
            
        `;
    }

    updated(changedProperties){
        if(changedProperties.has("showCarrusel")){
            this.toggleShowCarrusel();
        }
    }

    getCochesData(){
        console.log("Obteniendo datos de coches...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición GET coches completada OK");
                this.coches = JSON.parse(xhr.responseText);
            }

        }
        xhr.open("GET", "http://localhost:8081/hackathon/v1/coches");
        xhr.send();
        console.log("Fin de getCochesData");
    }

    toggleShowCarrusel(){
        if(this.showCoches)
            this.shadowRoot.getElementById("carruselCoches").classList.remove("d-none"); 
        else
            this.shadowRoot.getElementById("carruselCoches").classList.add("d-none");
    }


}

customElements.define('coches-carrusel', CochesCarrusel);