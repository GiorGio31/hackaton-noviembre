import {LitElement, html} from 'lit-element';

import '../coches-reserva/coches-reserva.js';


class CochesListado extends LitElement {
    
    static get properties(){
        return {
            reservas: {type: Array},
            showReservas: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.reservas = [];
        this.reservas2 = [
            {
                id: "12345",
                fecha: "01/01/2021",
                hora: "10",
                origen: "Tres Cantos",
                destino: "Ciudad BBVA",
                matricula: "1234BBB",
                nombre: "Pepito Perez"
                
            },
            {
                id: "67890",
                fecha: "15/07/2021",
                hora: "12",
                origen: "Las Tablas",
                destino: "Vaguada",
                matricula: "5678CCC",
                nombre: "Juanito Gonzalez"
                
            }
        ];
        this.showReservas=false;
        this.getReservasData();
    }
    
    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">
            
            <div id="listadoReservas">
            <div class="row">
                <div class="row row-cols-1 row-cols-sm-2">
                    ${this.reservas.map( reserva =>  html`<coches-reserva 
                        id="${reserva.id}" 
                        fecha="${reserva.fecha}"
                        hora="${reserva.hora}"
                        origen="${reserva.origen}"
                        destino="${reserva.destino}"
                        matricula="${reserva.matricula}"
                        nombre="${reserva.nombre}"
                        @cancelar-reserva="${this.cancelaReserva}"
                        ></coches-reserva>`)}
                </div>
                
            </div>  
            <br>  
                <button class="btn btn-secondary" @click="${this.getReservasData}">Refrescar Reservas</button>
            </div>
        `;
    }

    getReservasData(){
        console.log("Obteniendo datos de resevas...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición de Reservas completada OK");
                this.reservas = JSON.parse(xhr.responseText);
            }
        }
        xhr.open("GET", "http://localhost:8080/hackathon/v1/reservas");
        xhr.send();
        console.log("Fin de getReservasData");
    }

    cancelaReserva(e){
        console.log("Borrando el id: "+e.detail.id);

        let xhr = new XMLHttpRequest();
        xhr.open("DELETE", "http://localhost:8080/hackathon/v1/reservas/"+e.detail.id);
        xhr.send();
        console.log("Fin de delete");

        this.getReservasData();

    }

    cargaDatosReservas(e){
        console.log("Datos de reservas cargados");
        this.reservas = e.detail.reservas;
    }

    updated(changedProperties){
        if(changedProperties.has("showReservas")){
            this.toggleShowReservas();
        }
    }

    toggleShowReservas(){
        if(this.showReservas)
            this.shadowRoot.getElementById("listadoReservas").classList.remove("d-none"); 
        else
            this.shadowRoot.getElementById("listadoReservas").classList.add("d-none");
    }


}

customElements.define('coches-listadoreservas', CochesListado);