import {LitElement, html} from 'lit-element';

class CochesReserva extends LitElement {

    static get properties(){
        return {
            id: {type: String},
            fecha: {type: String},
            hora: {type: String},
            origen: {type: String},
            destino: {type: String},
            matricula: {type: String},
            nombre: {type: String}
        }
    }

    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
        crossorigin="anonymous">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-header">Reserva nº${this.id}</h5>
                    <h6 class="card-title">Programada para ${this.fecha} a las ${this.hora}h</h6>
                    <h6 class="card-title">Trayecto: ${this.origen} -> ${this.destino}</h6>
                    <h6 class="card-title">Vehiculo: ${this.matricula}</h6>
                    <h6 class="card-title">Nombre: ${this.nombre}</h6>
                </div>
                <div class="card-footer">
                    <button class="btn btn-info" @click="${this.cancelarReserva}">Cancelar</button>
                </div>
                </div>
            </div>
        `;
    }
    //

    cancelarReserva(){
        console.log("Reserva cancelada, emitiendo evento")

        this.dispatchEvent(new CustomEvent("cancelar-reserva", {
            detail: {
                id: this.id
            }
        }))

    }
}

customElements.define('coches-reserva', CochesReserva);