import {LitElement, html} from 'lit-element';

class CochesListadoReservasDM extends LitElement {
    
    static get properties(){
        return {
            reservas: {type: Array},
            reservas2: {type: Array},
            borraId: {typpe: String}
        };
    }

    constructor(){
        super();

        this.borraId = "";

        this.reservas2 = [];
        this.reservas = [
            {
                id: "12345",
                fecha: "01/01/2021",
                hora: "10",
                origen: "Tres Cantos",
                destino: "Ciudad BBVA",
                matricula: "1234BBB",
                nombre: "Pepito Perez"
                
            },
            {
                id: "67890",
                fecha: "15/07/2021",
                hora: "12",
                origen: "Las Tablas",
                destino: "Vaguada",
                matricula: "5678CCC",
                nombre: "Juanito Gonzalez"
                
            }
        ];

        //this.getReservasData();
    }


    updated(changedProperties){
        if(changedProperties.has("reservas")){

            this.dispatchEvent(new CustomEvent("carga-datos-reservas", {
                detail: {
                    reservas: this.reservas
                }
            }))
        }
        if(changedProperties.has("borraid")){
            if(borraid != ""){
                this.deleteReserva(borraid);
            }
        }
    }
}

customElements.define('coches-listadoreservas-dm', CochesListadoReservasDM);