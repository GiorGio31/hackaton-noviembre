import {LitElement, html} from 'lit-element';

class CochesReservasDM extends LitElement {
    
    static get properties(){
        return {
            reservas: {type: Array},
            reservas2: {type: Array}
        };
    }

    constructor(){
        super();

        this.reservas = [];
        this.reservas2 = [
            {
                id: "12345",
                fecha: "01/01/2021",
                hora: "10",
                origen: "Tres Cantos",
                destino: "Ciudad BBVA",
                matricula: "1234BBB",
                nombre: "Pepito Perez"
                
            },
            {
                id: "67890",
                fecha: "15/07/2021",
                hora: "12",
                origen: "Las Tablas",
                destino: "Vaguada",
                matricula: "5678CCC",
                nombre: "Juanito Gonzalez"
                
            },
        ];

        this.getReservasData();
        this.prueba();
    }

    getReservasData(){
        console.log("Obteniendo datos de resevas...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición completada");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.reservas = APIResponse.results;       
            }
        }

        xhr.open("GET", "https://localhost:");
        //El body de la petición se manda en el send. GET no tiene body, pero POST tendría.
        xhr.send();
        console.log("Fin de getReservasData");
    }

    prueba(){
        console.log("Pruebas...");
        console.log(reservas2);
        console.log(reservas);
        


    }
    
    updated(changedProperties){
        console.log("cargando listado de reservas en coches-reservas-dm");

        if(changedProperties.has("reservas")){

            this.dispatchEvent(new CustomEvent("carga-datos-reservas", {
                detail: {
                    reservas: this.reservas
                    reservas2: this.reservas2
                }
            }))
        }
    }
}

customElements.define('coches-reservas-dm', CochesReservasDM);